import React, { useEffect, useState } from 'react';
import { Link } from 'gatsby';

import { Socials } from './Socials';

import theAmazingDragon from '../../images/the-amazing-dragon.svg';
import theAmazingDragonFiring from '../../images/the-amazing-dragon-firing.svg';
import gradientDot from '../../images/gradient-dot.svg';

import './NavbarDesktop.sass';

function createLinkElements(links) {
  return links.map((link) => {
    const { title, slug } = link;

    const [isCurrentPage, setIsCurrentPage] = useState(false);
    useEffect(() => {
      setIsCurrentPage(slug === window.location.pathname);
    }, []);

    return (
      <li
        className={`navbar--desktop__links__link ${
          isCurrentPage ? 'navbar--desktop__links__link--current-page' : ''
        }`}
      >
        {isCurrentPage && (
          <img
            className="navbar--desktop__links__link--current-page__dot"
            src={gradientDot}
          />
        )}
        <Link to={slug}>{title}</Link>
      </li>
    );
  });
}

function toggleDragonFiring({ activeDragon, setActiveDragon }) {
  setActiveDragon(
    activeDragon === theAmazingDragon
      ? theAmazingDragonFiring
      : theAmazingDragon
  );
}

export const NavbarDesktop = ({ links }) => {
  const linkElements = createLinkElements(links);

  const [activeDragon, setActiveDragon] = useState(theAmazingDragon);

  return (
    <div className="navbar--desktop">
      <img
        className="navbar--desktop__amazing-dragon"
        src={activeDragon}
        onMouseEnter={() =>
          toggleDragonFiring({ activeDragon, setActiveDragon })
        }
        onMouseLeave={() =>
          toggleDragonFiring({ activeDragon, setActiveDragon })
        }
      />
      <nav>
        <ul className="navbar--desktop__links">{linkElements}</ul>
      </nav>
      <Socials />
    </div>
  );
};

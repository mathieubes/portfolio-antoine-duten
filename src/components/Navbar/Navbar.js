import React from 'react';

import {useIsMobile} from "../../hooks/useIsMobile";

import {NavbarDesktop} from "./NavbarDesktop";
import {NavbarMobile} from "./NavbarMobile";


export const Navbar = ({ links }) => {
  const compatibleNavbar = useIsMobile() ? <NavbarMobile links={links} /> : <NavbarDesktop links={links} />;
  return compatibleNavbar;
};

import React from 'react';

import {StringUtils} from "../../utils/string.utils";

import hiEmoji from '../../images/emojis/hi.svg';
import emailEmoji from '../../images/emojis/email.svg';
import phoneEmoji from '../../images/emojis/phone.svg';

import './Socials.sass';

const PHONE_NUMBER = '0635992342';

export const Socials = () => {
  return (
    <div className="socials">
      {/* Hi */}
      <div className="socials__hi">
        <img src={hiEmoji} />
        Say hi !
      </div>

      {/* Email */}
      <a href="mailto:a.duten@gmail.com" className="btn btn--secondary">
        <img src={emailEmoji} />
        a.duten@gmail.com
      </a>

      {/* Phone number */}
      <a
        href={`tel:+33${PHONE_NUMBER.substring(1, PHONE_NUMBER.length)}`}
        className={`btn btn--secondary`}
      >
        <img src={phoneEmoji} />
        {StringUtils.formatPhoneNumber(PHONE_NUMBER)}
      </a>
    </div>
  );
};

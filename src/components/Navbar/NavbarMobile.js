import React, {useEffect, useState} from 'react';

import {Link} from "gatsby";

import gradientDot from "../../images/gradient-dot.svg";
import activeDragon from '../../images/the-amazing-dragon.svg';

import './NavbarMobile.sass';

function createLinkElements(links) {
  return links.map((link) => {
    const { title, slug } = link;

    const [isCurrentPage, setIsCurrentPage] = useState(false);
    useEffect(() => {
      setIsCurrentPage(slug === window.location.pathname);
    }, [])

    return (
      <li className={`${isCurrentPage ? 'navbar--mobile__navigation--current-page' : ''}`}>
        {isCurrentPage && <img src={gradientDot} />}
        <Link to={slug}>{title}</Link>
      </li>
    );
  });
}

function toggleOpenState(isOpen, setIsOpen) {
  setIsOpen(!isOpen);
}

export const NavbarMobile = ({ links }) => {
  const linkElements = createLinkElements(links);

  const [isOpen, setIsOpen] = useState(false);

  return (
    <div className={`navbar--mobile ${isOpen ? 'navbar--mobile--open' : ''}`}>
      {/* TODO Add so what ? */}
      <img
        className={`navbar--mobile__amazing-dragon ${isOpen ? 'navbar--mobile__amazing-dragon--menu-open' : ''}`}
        src={activeDragon}
      />

      <button
        className={`navbar--mobile__burger-button ${isOpen ? 'navbar--mobile__burger-button--menu-open' : ''}`}
        onClick={() => toggleOpenState(isOpen, setIsOpen)}
      >
        <span></span>
        <span></span>
        <span></span>
      </button>

      <nav className={`navbar--mobile__navigation ${isOpen ? 'navbar--mobile__navigation--menu-open' : ''}`}>
        <ul>{linkElements}</ul>
      </nav>
    </div>
  );
};

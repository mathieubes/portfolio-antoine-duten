import React, { useEffect } from 'react';
import gsap from 'gsap/all';

import pdfIcon from '../../images/pdf-icon.svg';
import chabuduoLogo from '../../images/chabuduo-logo.svg';

import { AnimationUtils } from '../../utils/gsap.utils';
import { StringUtils } from '../../utils/string.utils';

import './Content.sass';

export const CONTENT_ANIMATION_ID = 'homepage-content';
export const HELLO_ANIMATION_ID = 'hello-there';
export const NAME_ANIMATION_ID = 'name';

export const Content = ({ cvURL, children }) => {
  useEffect(() => {
    gsap.from(
      StringUtils.getAnimateSelectorAndMore(CONTENT_ANIMATION_ID, '*'),
      {
        opacity: 0,
        skewY: 5,
        x: 50,
        stagger: 0.1,
      }
    );
  }, []);

  return (
    <div animation-id={CONTENT_ANIMATION_ID} className="homepage-content">
      <div animation-id={HELLO_ANIMATION_ID}>Hello there!</div>
      <div animation-id={NAME_ANIMATION_ID} className="homepage-content__title">
        I'm <span className="homepage-content__title__name">Antoine Duten</span>
      </div>
      <div>UX | UI &amp; Product Designer</div>
      <div className="homepage-content__description">{children}</div>
      <p className="homepage-content__surname">
        I work freelance under the name of...
        <img
          className="homepage-content__surname__logo"
          style={{ verticalAlign: 'middle' }}
          src={chabuduoLogo}
        />
      </p>
      <div className="homepage-content__buttons">
        <a href={cvURL} target="_blank" className="btn">
          <img src={pdfIcon} />
          See my cv
        </a>
        {/* <a href="#" className="btn btn--outlined">
          See my projects
        </a> */}
      </div>
    </div>
  );
};

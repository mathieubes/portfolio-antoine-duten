import React, { useEffect, useState } from 'react';

import { StoreUtils } from '../utils/store.utils';
import { AnimationUtils } from '../utils/gsap.utils';

import { SEO } from './SEO/SEO';
import { Navbar } from './Navbar/Navbar';
import {
  UnderConstructionBanner,
  UNDER_CONSTRUCTION_BANNER_ANIMATION_ID,
} from './UnderConstruction/UnderConstructionBanner';

import openGraphImage from '../images/og-image.jpeg';

import '../sass/layout.sass';

const TAB_TITLE = 'Antoine DUTEN - Portfolio';

const NAVBAR_LINKS = [{ title: 'Home', slug: '/' }];

export const Layout = ({ className, children }) => {
  const [showBanner, setShowBanner] = useState(false);

  const closeUnderConstructionBanner = () => {
    AnimationUtils.destroy(
      UNDER_CONSTRUCTION_BANNER_ANIMATION_ID,
      { y: 100 },
      () => {
        setShowBanner(false);
        StoreUtils.updateUnderMessageState();
      }
    );
  };

  useEffect(() => {
    setShowBanner(!StoreUtils.hasAlreadySeenUnderConstructionMessage());
  }, []);

  return (
    <>
      {/* Maybe move this SEO component to another component or put a seo attribut in params */}
      <SEO title={TAB_TITLE} image={openGraphImage} />
      <Navbar links={NAVBAR_LINKS} />
      <main className={className}>{children}</main>
      {showBanner && (
        <UnderConstructionBanner closeCallback={closeUnderConstructionBanner} />
      )}
    </>
  );
};

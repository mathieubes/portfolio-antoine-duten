import React, { useEffect } from 'react';

import bearEmoji from '../../images/emojis/bear.svg';
import closeIcon from '../../images/x-circle.svg';
import { AnimationUtils } from '../../utils/gsap.utils';

import './UnderConstructionBanner.sass';

export const UNDER_CONSTRUCTION_BANNER_ANIMATION_ID =
  'under-construction-banner';

export const UnderConstructionBanner = ({ closeCallback }) => {
  useEffect(() => {
    AnimationUtils.show(UNDER_CONSTRUCTION_BANNER_ANIMATION_ID, {
      y: 100,
      delay: 2,
    });
  }, []);

  return (
    <div
      animation-id={UNDER_CONSTRUCTION_BANNER_ANIMATION_ID}
      className={'under-construction-banner'}
    >
      <button
        className={'under-construction-banner__close'}
        onClick={closeCallback}
      >
        <img src={closeIcon} />
      </button>
      <img src={bearEmoji} />
      <strong>This portfolio is still under construction</strong>, please bear
      with me: it'll be fully released out in the wild web soon!
    </div>
  );
};

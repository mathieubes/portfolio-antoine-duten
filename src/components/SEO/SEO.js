import React from 'react';
import { Helmet } from 'react-helmet';

export const SEO = ({ title, image }) => {
  return (
    <Helmet title={title}>
      <meta name="image" content={image} />

      {/* Open Graph */}
      <meta property="og:title" content={title} />
      <meta property="og:image" content={image} />
    </Helmet>
  );
};

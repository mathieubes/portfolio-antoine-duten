import { gsap } from 'gsap/all';

import { StringUtils } from './string.utils';

export class AnimationUtils {
  static show(
    animationId,
    { x, y, opacity, scale, duration, delay, stagger, ease }
  ) {
    gsap.from(StringUtils.getAnimateSelector(animationId), {
      x,
      y,
      opacity: opacity || 0,
      scale,
      duration: duration || 0.5,
      delay,
      stagger,
      ease: ease || 'power3.in',
    });
  }

  static destroy(animationId, { opacity, y }, callback) {
    gsap.to(StringUtils.getAnimateSelector(animationId), {
      opacity: opacity || 0,
      y: y || 0,
      duration: 0.5,
      ease: 'power3.in',
      onComplete: callback,
    });
  }
}

const UNDER_CONSTRUCTION_KEY = 'under_construction_message';

export class StoreUtils {
  static hasAlreadySeenUnderConstructionMessage() {
    return localStorage.getItem(UNDER_CONSTRUCTION_KEY) ? true : false;
  }

  static updateUnderMessageState() {
    localStorage.setItem(UNDER_CONSTRUCTION_KEY, 'true');
  }
}

export class StringUtils {
  static formatPhoneNumber = (phoneNumber) => {
    let formattedPhoneNumber = '';
    for (let i = 0; i < phoneNumber.length; i += 2)
      formattedPhoneNumber += phoneNumber.slice(i, i + 2) + ' ';

    return formattedPhoneNumber.trimEnd();
  };

  static getAnimateSelector = (id) => {
    return `[animation-id="${id}"]`;
  };

  static getAnimateSelectorAndMore = (id, more) => {
    return `[animation-id="${id}"]>${more}`;
  };
}

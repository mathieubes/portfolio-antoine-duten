import React, { useEffect } from 'react';
import { graphql, useStaticQuery } from 'gatsby';

import { Layout } from '../components/Layout';
import { Content } from '../components/Homepage/Content';

import { useIsMobile } from '../hooks/useIsMobile';

import { AnimationUtils } from '../utils/gsap.utils';

import { MainIllustration } from '../components/SVG/MainIllustration';

import './index.sass';

export const MAIN_ILLUSTRATION_ANIMATION_ID = 'main-illustration';

const GOBELINS_CONTENT = (
  <>
    Also alumnus <a href="https://gobelins.fr/">@Gobelins</a>, L'Ecole de
    l'Image,
    <br />
    #1st UX Master Program in France
  </>
);

const query = graphql`
  query {
    data: strapiHomepage {
      cv {
        localFile {
          url
        }
      }
    }
  }
`;

const IndexPage = () => {
  const { data } = useStaticQuery(query);

  useEffect(() => {
    AnimationUtils.show(MAIN_ILLUSTRATION_ANIMATION_ID, {
      scale: 0.6,
      duration: 2,
      ease: 'back',
    });
  }, []);

  return (
    <Layout className="homepage">
      <MainIllustration className="homepage__illustration" />

      <Content cvURL={data.cv.localFile.url}>
        {!useIsMobile() && GOBELINS_CONTENT}
      </Content>
    </Layout>
  );
};

export default IndexPage;

require('dotenv').config();

module.exports = {
  siteMetadata: {
    title: 'Antoine Duten - Portfolio',
    siteUrl: 'https://www.antoine-duten.fr',
    image: '/og-image.jpeg',
  },
  plugins: [
    'gatsby-plugin-sass',
    {
      resolve: 'gatsby-source-strapi',
      options: {
        apiURL: process.env.API_URL,
        singleTypes: ['homepage'],
        queryLimit: 1000,
      },
    },
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: 'Antoine Duten - Portfolio',
        short_name: 'Antoine Duten',
        start_url: '/',
        icon: 'src/images/favicon.png',
      },
    },
    `gatsby-plugin-react-helmet`,
  ],
};
